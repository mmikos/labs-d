/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabD.common;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Dictionary;
import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

/**
 *
 * @author Mariusz
 */
public class Animal1 implements Animal {

    String gatunek;  
    String name;
    private String status;
    private ComponentContext context;
    PropertyChangeSupport pcs;
    Logger logger; 
    
    public Animal1(){
        pcs = new PropertyChangeSupport(this);
    }
    
    public void activate(ComponentContext context){
        this.context = context;
        
        Dictionary slownik = context.getProperties();
        
        this.name = (String)slownik.get("name");
        this.gatunek =(String) slownik.get("gatunek");
        
        logger.log(this, "Activated");
        System.out.println("activate");
    }
    
    public void deactivate(){
        logger.log(this, "Deactivated");
        System.out.println("deactivate");
    }
    
    public void setLogger(Logger logger){
        this.logger = logger;
    }
    
    @Override
    public String getSpecies() {
        return gatunek; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getStatus() {
        return status; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setStatus(String status) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
